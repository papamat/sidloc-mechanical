# SIDLOC Mechanical

Mechanical parts that enclosing the electronics of the SIDLOC beacon.

## Instructions

git lfs clone

last version is built with:  
OS: Fedora Linux 35 (Workstation Edition) (GNOME/gnome)  
Word size of FreeCAD: 64-bit  
Version: 2022.430.26244 +4758 (Git) AppImage  
Build type: Release  
Branch: LinkDaily  
Hash: b024b876480a533018fdc702145e9e38f8c54ba6  
Python version: 3.9.12  
Qt version: 5.12.9  
Coin version: 4.0.1  
OCC version: 7.5.3  
Locale: English/United States (en_US)  


## License

Licensed under the [CERN OHLv1.2](LICENSE) 2018 Libre Space Foundation.